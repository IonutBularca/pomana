#include "Graph.h"

#include <iostream>


void main() {
	

	Graph graf;
	graf.importGraph("nodes.node", "connections.conn");

	std::cout << graf << std::endl;

	graf.dijkstra(1, 6);

	graf.aStar(1, 6);
}