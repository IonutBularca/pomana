#pragma once
#include <string>
#include <iostream>
class Node
{
public:
	Node();
	~Node();
	Node(const int x, const int y, const std::string& name);
	int getId() const { return _id; };

	friend std::ostream& operator << (std::ostream& os, const Node& obj);

	float distanceTo(const Node& other) const;


public:
	std::string _name;
	int _x, _y;
private:
	int _id;
};
