#pragma once
#include "Node.h"

#include <iostream>
#include <vector>
#include <map>

class Graph
{
public:
	Graph();
	~Graph();


	void addNode(const Node& node);

	void addConnection(const int ID1, const int ID2);


	friend std::ostream& operator << (std::ostream& os , const Graph& obj);

	void importGraph(const std::string& nodes, const std::string& connections);

	//-------------------------------------------------------------------------------------------------------------------

	void dijkstra(const int startID, const int stopId);

	void aStar(const int startId, const int stopId);

	//-------------------------------------------------------------------------------------------------------------------

public:
	std::map<int, Node> _nodesAndIDs;
	std::map<int, std::vector<Node>> _connections;
	int _numberOfNodes = 0;
};

