#include "Node.h"



Node::Node()
{
}


Node::~Node()
{
}

Node::Node(const int x, const int y, const std::string& name)
{
	static int currentId = 1;
	_id = currentId;
	_name = name;
	_x = x;
	_y = y;
	currentId++;
}

std::ostream& operator<<(std::ostream& os, const Node& obj)
{
	os << "ID: " << obj._id << " Name: " << obj._name << " Coord: " << obj._x << ","<<obj._y;

	return os;
}


float Node::distanceTo(const Node& other) const{
	return sqrt((other._x - _x)* (other._x - _x) + (other._y - _y) * (other._y - _y));
}