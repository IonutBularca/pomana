#include "Graph.h"

#include <fstream>

#include <queue>

Graph::Graph()
{
}


Graph::~Graph()
{
}

void Graph::addNode(const Node& node)
{
	if (_connections.find(node.getId()) != _connections.end()) {
		throw "The node is already in the graph";
	}
	_numberOfNodes++;
	_connections[node.getId()] = std::vector<Node>();
	_nodesAndIDs[node.getId()] = node;
}

void Graph::addConnection(const int ID1, const int ID2)
{
	if (_nodesAndIDs.find(ID1) == _nodesAndIDs.end() || _nodesAndIDs.find(ID1) == _nodesAndIDs.end())
		throw "The IDs you entered are not valid";
	_connections[ID1].push_back(_nodesAndIDs[ID2]);
	_connections[ID2].push_back(_nodesAndIDs[ID1]);
}

void Graph::importGraph(const std::string& nodes, const std::string& connections)
{

	std::ifstream nodesIn(nodes);
	std::ifstream connIn(connections);

	int numNodes;
	int numConn;
	nodesIn >> numNodes;
	connIn >> numConn;

	for (int i = 0; i < numNodes; i++) {
		int x, y;
		std::string name;
		nodesIn >> name >> x >> y;
		Node node(x, y, name);
		addNode(node);
	}

	for (int i = 0; i < numConn; i++) {
		int id1, id2;
		connIn >> id1 >> id2;
		addConnection(id1, id2);
	}

}

std::ostream& operator<<(std::ostream& os, const Graph& obj)
{
	os << "Nodes:" << std::endl;
	for (const auto& node : obj._nodesAndIDs) {
		os << node.second << std::endl;
	}
	
	os << "Connections:" << std::endl;

	for (const auto& connection : obj._connections) {
		os << "Node(" << connection.first << ")("<< obj._nodesAndIDs.at(connection.first)._name <<"): ";
		for (const auto& nodeId : connection.second) {
			os << nodeId._name << "("<< nodeId.getId() << ") ";
		}
		os << std::endl;
	}

	return os;
}




void Graph::dijkstra(const int startID, const int stopId) {
	std::vector<float> dist;
	dist.resize(_numberOfNodes +1);
	std::vector<int> pred;
	pred.resize(_numberOfNodes +1);

	for (int i = 0; i <= _numberOfNodes; i++) {
		dist[i] = INT_MAX;
		pred[i] = -1;
	}

	dist[startID] = 0;
	pred[startID] = -2;

	auto comp = [&dist](const Node & n1, const Node & n2) {
		return dist[n1.getId()] > dist[n2.getId()];
	};

	std::priority_queue < Node,std::vector<Node>, decltype(comp) > queue(comp);

	int nNodes = 0;

	queue.push(_nodesAndIDs[startID]);

	while (!queue.empty()) {
	
		for (const auto& conn : _connections[queue.top().getId()]) {
			if (pred[conn.getId()] == -1) {
				pred[conn.getId()] = queue.top().getId();
				dist[conn.getId()] = dist[queue.top().getId()] + conn.distanceTo(queue.top());
				queue.push(conn);
			}
		}
		if (queue.top().getId() == stopId) {
			break;
		}
		queue.pop();
		nNodes++;
	}

	int current = stopId;
	while (current != -2) {
		std::cout << current << " ";
		current = pred[current];
	}

	std::cout << std::endl << "Noduri analizate: " << nNodes << std::endl;

}


void Graph::aStar(const int startID, const int stopId) {
	std::vector<float> dist;
	dist.resize(_numberOfNodes + 1);
	std::vector<int> pred;
	pred.resize(_numberOfNodes + 1);

	for (int i = 0; i <= _numberOfNodes; i++) {
		dist[i] = INT_MAX;
		pred[i] = -1;
	}

	dist[startID] = 0;
	pred[startID] = -2;

	auto comp = [&](const Node & n1, const Node & n2) {
		return (dist[n1.getId()] + n1.distanceTo(_nodesAndIDs[stopId])) > (dist[n2.getId()] + n2.distanceTo(_nodesAndIDs[stopId]));
	};

	std::priority_queue < Node, std::vector<Node>, decltype(comp) > queue(comp);

	int nNodes = 0;

	queue.push(_nodesAndIDs[startID]);

	while (!queue.empty()) {

		for (const auto& conn : _connections[queue.top().getId()]) {
			if (pred[conn.getId()] == -1) {
				pred[conn.getId()] = queue.top().getId();
				dist[conn.getId()] = dist[queue.top().getId()] + conn.distanceTo(queue.top());
				queue.push(conn);
			}
		}
		if (queue.top().getId() == stopId) {
			break;
		}
		queue.pop();
		nNodes++;
	}

	int current = stopId;
	while (current != -2) {
		std::cout << current << " ";
		current = pred[current];
	}

	std::cout << std::endl << "Noduri analizate: " << nNodes << std::endl;

}